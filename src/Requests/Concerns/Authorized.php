<?php

/**
 * This file is part of Cdek SDK package.
 *
 * © Appwilio (http://appwilio.com), greabock (https://github.com/greabock), JhaoDa (https://github.com/jhaoda)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace LaptopDev\okradomApi\Requests\Concerns;

use LaptopDev\okradomApi\Contracts\ShouldAuthorize;

trait Authorized
{
    /**
     * @JMS\XmlAttribute
     * @JMS\SerializedName("Inn")
     * @JMS\Type("string")
     *
     * @var string
     */
    public $account;

    /**
     * @JMS\XmlAttribute
     * @JMS\SerializedName("Password")
     *
     * @var string
     */
    public $secure;

    public function credentials(string $account, string $secure): ShouldAuthorize
    {
        $this->account = $account;
        $this->secure = $secure;

        /** @var ShouldAuthorize $this */
        return $this;
    }
}
