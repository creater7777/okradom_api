<?php
/**
 * Created by PhpStorm.
 * User: Professional
 * Date: 19.11.2021
 * Time: 2:45
 */

namespace LaptopDev\okradomApi\Requests\Concerns;

use LaptopDev\okradomApi\Contracts\ShouldAuthorize;
use JMS\Serializer\Annotation as JMS;

/**
 */
abstract class BaseRequest
{
    protected const METHOD = 'POST';

    /**
     * @JMS\SerializedName("Inn")
     * @JMS\XmlElement(cdata=false, namespace="http://www.okradom.ru")
     * @JMS\Type("string")
     *
     * @var string
     */
    public $account;

    /**
     * @JMS\SerializedName("Password")
     * @JMS\XmlElement(cdata=false, namespace="http://www.okradom.ru")
     * @JMS\Type("string")
     *
     * @var string
     */
    public $secure;


    abstract public function getSOAPAction(): string;

    abstract public function getAddress(): string;

    public function __construct($data = null)
    {
        if (!empty($data)){
            foreach ($data as $key => $value) {
                if (property_exists($this, $key)) {
                    $this->{$key} = $value;
                }
            }
        }
    }

    public function credentials(string $account, string $secure): ShouldAuthorize
    {
        $this->account = $account;
        $this->secure = $secure;

        /** @var ShouldAuthorize $this */
        return $this;
    }
    public static function create($data)
    {
        return new static($data);
    }

    public function getMethod(): string
    {
        return static::METHOD;
    }
}
