<?php
/**
 * Created by PhpStorm.
 * User: Professional
 * Date: 20.11.2021
 * Time: 7:30
 */

namespace LaptopDev\okradomApi\Requests\Concerns;

use JMS\Serializer\Annotation as JMS;

class Body
{

    /**
     * @JMS\XmlElement(cdata=false, namespace="http://www.w3.org/2003/05/soap-envelope")
     * @JMS\SerializedName("Body")
     */
    public $content;

    public function __construct($request){
        $this->content = $request;
    }
}
