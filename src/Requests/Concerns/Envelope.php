<?php
/**
 * Created by PhpStorm.
 * User: Professional
 * Date: 20.11.2021
 * Time: 5:53
 */
namespace LaptopDev\okradomApi\Requests\Concerns;

use JMS\Serializer\Annotation as JMS;
use LaptopDev\okradomApi\Contracts\XmlRequest;

/**
 * @JMS\XmlRoot("soap:Envelope")
 * @JMS\XmlNamespace(uri="http://www.okradom.ru", prefix="okr")
 * @JMS\XmlNamespace(uri="http://www.w3.org/2003/05/soap-envelope", prefix="soap")
 */

class Envelope implements XmlRequest {

    /**
     * @JMS\XmlElement(cdata=false, namespace="http://www.w3.org/2003/05/soap-envelope")
     * @JMS\SerializedName("Body")
     *
     * @var Body $body
     */
    public $body;

    public function __construct(XmlRequest $request, $account, $pass) {
        $request->account = $account;
        $request->secure = $pass;
        $this->body = new Body($request);
    }

    public function __call($name, $arguments)
    {
        if (method_exists($this->body, $name)){
            return $this->body->$name(...$arguments);
        }
        return null;
    }

    public function getMethod(): string
    {
        return $this->body->getMethod();
    }

    public function getAddress(): string
    {
        return $this->body->getAddress();
    }
}

/**
 * @JMS\XmlRoot("soap:Body")
 */
class Body implements XmlRequest{

    /**
     * @JMS\SerializedName($this->entity::class)
     */
    public $entity;

    public function __construct($request){
        $this->entity = $request;
    }

    public function getMethod(): string
    {
        // TODO: Implement getMethod() method.
        return "";
    }

    public function getAddress(): string
    {
        // TODO: Implement getAddress() method.
        return "";
    }
}
