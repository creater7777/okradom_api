<?php
/**
 * Created by PhpStorm.
 * User: Professional
 * Date: 18.11.2021
 * Time: 3:50
 */

namespace LaptopDev\okradomApi\Requests;

use LaptopDev\okradomApi\Contracts\XmlRequest;
use LaptopDev\okradomApi\Requests\Concerns\BaseRequest;
use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\XmlRoot("okr:GetTheGroups")
*/
class GetTheGroups extends BaseRequest implements XmlRequest
{
    protected const METHOD = 'POST';
    protected const ACTION = 'GetTheGroups';

//    /**
//     * @JMS\Exclude
//     *
//     * @var PrintReceiptsError[]
//     */
//    protected $errors;

    /**
     * @JMS\XmlElement(cdata=false, namespace="http://www.okradom.ru")
     * @JMS\SerializedName("ИнформацияОбОшибке")
     * @JMS\Type("string")
     */
    protected $error = '';

    /**
     * @JMS\XmlElement(cdata=false, namespace="http://www.okradom.ru")
     * @JMS\SerializedName("ID")
     * @JMS\Type("string")
     */
    protected $id = '';

    /**
     * @JMS\XmlElement(cdata=false, namespace="http://www.okradom.ru")
     * @JMS\SerializedName("GUIDЗаказа")
     * @JMS\Type("string")
     */
    protected $GUID = '';

    /**
     * @JMS\XmlElement(cdata=false, namespace="http://www.okradom.ru")
     * @JMS\SerializedName("Согласован")
     * @JMS\Type("boolean")
     */
    protected $approved = 0;


    public function getAddress(): string
    {
        return "";
    }

    public function getSOAPAction(): string
    {
        return "";
    }
}
