<?php

/**
 * This file is part of Cdek SDK package.
 *
 * © Appwilio (http://appwilio.com), greabock (https://github.com/greabock), JhaoDa (https://github.com/jhaoda)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace LaptopDev\okradomApi\Responses;

use LaptopDev\okradomApi\Common\Order;
use JMS\Serializer\Annotation as JMS;

/**
 * Class InfoReportResponse
 *
 * @package LaptopDev\okradomApi\Responses
 */
class InfoReportResponse
{
    /**
     * @JMS\XmlList(entry = "Order", inline = true)
     * @JMS\Type("array<LaptopDev\okradomApi\Common\Order>")
     *
     * @var array|Order[]
     */
    protected $orders = [];

    /**
     * @return Order[]|array
     */
    public function getOrders()
    {
        return $this->orders;
    }
}
